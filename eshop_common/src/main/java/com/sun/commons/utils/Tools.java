package com.sun.commons.utils;

/**
 * @author sunhongmin
 *	工具类
 */
public class Tools {

	/**
	 * 判断数组是否包含某元素
	 * @param <T>
	 *   **需要保证类型统一  （例如  拿Integer.equals(Long)即使值相同也会报错
	 * @param arr
	 * @param targetValue
	 * @return
	 */
	public static <T> boolean indexOf(T[] source_array, T search_value) {
		for (T s : source_array) {
			//需要保证类型统一  （例如  拿Integer.equals(Long)即使值相同也会报错
			if (s.equals(search_value)) {
				return true;
			}
		}
		return false;
	}
}
