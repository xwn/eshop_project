package com.sun.passport.controller;

import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.utils.ActionUtils;
import com.sun.commons.utils.CookieUtils;
import com.sun.commons.utils.JsonUtils;
import com.sun.passport.service.TbUserService;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbUser;

@Controller
public class PassportController {
	
	@Resource
	private TbUserService tbUserService;
	@Resource
	private RedisDao redisDao;

	@RequestMapping("/user/showLogin")
	public String showLogin(@RequestHeader(value="Referer",defaultValue="")String redirect,Model retmodel,String cartUrl){
		if(cartUrl!=null && !cartUrl.equals("")){
			//cartUrl优先级最高，不为空则优先跳转
			retmodel.addAttribute("redirect", cartUrl);
		}else{
			//如果cartUrl为空，则由请求头Referer决定，当Referer也为空时，会跳转到商城首页
			retmodel.addAttribute("redirect", redirect);
		}
		return "login";
	}
	
	@ResponseBody
	@RequestMapping("/user/login")
	public Map<String,Object> login(String username,String password,HttpServletRequest request,HttpServletResponse response){
		TbUser user = tbUserService.loadTbUserByUsernameAndPassword(username, password);
		if(user==null){
			return ActionUtils.ajaxFailMsg("", "用户名或密码错误");
		}
		
		user.setPassword("");
		String randomUUID = UUID.randomUUID().toString();
		redisDao.setKey(randomUUID, JsonUtils.objectToJson(user));
		
		//设置cookie 有效期一周
		//CookieUtils.setCookie(request, response, "TT_TOKEN", randomUUID, 60*60*24*7);
		//redisDao.expire(randomUUID, 60*60*24*7);
		
		//设置cookie 默认有效期  redis key有效期3小时
		CookieUtils.setCookie(request, response, "TT_TOKEN", randomUUID);
		//redisDao.expire(randomUUID, 60*60*24);
		redisDao.expire(randomUUID, 60*60*3);
		
		return ActionUtils.ajaxSuccessMsg(randomUUID, "OK");
	}
	
	@ResponseBody
	@RequestMapping("/user/token/{token}")
	public Object getUserInfoByToken(@PathVariable String token,String callback){
		Map<String, Object> resultMap = tbUserService.getUserInfoByToken(token);
		if(callback!=null && !callback.equals("")){
			MappingJacksonValue jsonp = new MappingJacksonValue(resultMap);
			jsonp.setJsonpFunction(callback);
			return jsonp;
		}
		return resultMap;
	}
	
	@ResponseBody
	@RequestMapping("/user/logout/{tocken}")
	public Object logout(@PathVariable String tocken,String callback){
		redisDao.del(tocken);
		if(callback!=null && !callback.equals("")){
			MappingJacksonValue jsonp = new MappingJacksonValue(ActionUtils.ajaxSuccessMsg("", "OK"));
			jsonp.setJsonpFunction(callback);
			return jsonp;
		}
		return ActionUtils.ajaxSuccessMsg("", "OK");
	}

}
